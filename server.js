let express = require("express");
let multer = require("multer");
let path = require("path");
let bodyParser = require("body-parser");

const ftp = require("basic-ftp") 
const fs = require("fs")
const app = express();

app.use(bodyParser.urlencoded({extended: true}));

const options = {
  key: fs.readFileSync('tls/private-key.pem'),
  cert: fs.readFileSync('tls/public-cert.pem'),
   rejectUnauthorized: false
};

let diskStorage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, "uploads");
  },
  filename: (req, file, callback) => {
    let filename = `${Date.now()}${file.originalname}`;
    callback(null, filename);
  }
});

let uploadFile = multer({storage: diskStorage}).single("file");

app.get("/", (req, res) => {
  res.sendFile(path.join(`${__dirname}/views/index.html`));
});

 
app.post("/upload", async(req, res) => {
  
  uploadFile(req, res, (error) => {
    if (error) {
      return res.send(`Error when trying to upload: ${error}`);
    }
  });
  const client = new ftp.Client()
  client.ftp.verbose = true
  try {
      await client.access({
          host: "192.168.1.4",
          port:21,
          user: "michael",
          password: "nam150498",
          secure: true,
          secureOptions:options
      })
      await client.uploadFromDir("./uploads","/uploads")
  }
  catch(err) {
      console.log(err)
  }
    
});
 
app.listen(8080, "localhost", () => {
  console.log(`Hello`);
});